from __future__ import division

import os
import sys, time, torch, random, argparse, PIL
import pprint
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True
from copy import deepcopy
from pathlib import Path
from shutil import copyfile
import numbers, numpy as np
sys.path.append(".")
lib_dir = (Path(__file__).parent / '..' / 'lib').resolve()
if str(lib_dir) not in sys.path: sys.path.insert(0, str(lib_dir))
sys.path.append("/home/mcw/Rohini/landmarks/training/HRNet-DT-Merge-BK-Matched224/lib/")
assert sys.version_info.major == 3, 'Please upgrade from {:} to Python 3.x'.format(sys.version_info)
from config_utils import obtain_lk_args as obtain_args
from procedure import save_checkpoint, lk_train as train, basic_eval_all as eval_all
from dataset import VideoDataset as VDataset, GeneralDataset as IDataset
from xvision import transforms
from log_utils import Logger, AverageMeter, time_for_file, convert_secs2time, time_string
from models import obtain_LK as obtain_model, remove_module_dict
from models.LK import LK
from optimizer import obtain_optimizer
from lib.config import config, update_config
from config_utils import load_configure
from lib.utils import utils
import lib.models as models
import torch.backends.cudnn as cudnn
from lib.datasets import get_dataset
from lib.datasets.face300w_val import Face300W_VAL
from tensorboardX import SummaryWriter
from lib.core import function
from warnings import filterwarnings
filterwarnings("ignore")

def parse_args():

    parser = argparse.ArgumentParser(description='Train Face Alignment')
    parser.add_argument('--cfg', help='experiment configuration filename', required=True, type=str)
    parser.add_argument('--train_lists',      type=str,   nargs='+',      help='The list file path to the video training dataset.')
    parser.add_argument('--eval_vlists',      type=str,   nargs='+',      help='The list file path to the video testing dataset.')
    parser.add_argument('--eval_ilists',      type=str,   nargs='+',      help='The list file path to the image testing dataset.')
    parser.add_argument('--num_pts',          type=int,                   help='Number of point.')
    parser.add_argument('--model_config',     type=str,                   help='The path to the model configuration')
    #parser.add_argument('--hrnet_config',     type=str, required=True,    help='experiment configuration filename')
    parser.add_argument('--opt_config',       type=str,                   help='The path to the optimizer configuration')
    parser.add_argument('--lk_config',        type=str,                   help='The path to the LK configuration')
    # Data Generation
    parser.add_argument('--heatmap_type',     type=str,   choices=['gaussian','laplacian'], help='The method for generating the heatmap.')
    parser.add_argument('--data_indicator',   type=str, default='300W-68',help='The dataset indicator.')
    parser.add_argument('--video_parser',     type=str,                   help='The video-parser indicator.')
    # Data Transform
    parser.add_argument('--pre_crop_expand',  type=float,                 help='parameters for pre-crop expand ratio')
    parser.add_argument('--sigma',            type=float,                 help='sigma distance for CPM.')
    parser.add_argument('--scale_prob',       type=float,                 help='argument scale probability.')
    parser.add_argument('--scale_min',        type=float,                 help='argument scale : minimum scale factor.')
    parser.add_argument('--scale_max',        type=float,                 help='argument scale : maximum scale factor.')
    parser.add_argument('--scale_eval',       type=float,                 help='argument scale : maximum scale factor.')
    parser.add_argument('--rotate_max',       type=int,                   help='argument rotate : maximum rotate degree.')
    parser.add_argument('--crop_height',      type=int,   default=256,    help='argument crop : crop height.')
    parser.add_argument('--crop_width',       type=int,   default=256,    help='argument crop : crop width.')
    parser.add_argument('--crop_perturb_max', type=int,                   help='argument crop : center of maximum perturb distance.')
    parser.add_argument('--arg_flip',         action='store_true',        help='Using flip data argumentation or not ')
    # Optimization options
    parser.add_argument('--eval_once',        action='store_true',        help='evaluation only once for evaluation ')
    parser.add_argument('--error_bar',        type=float,                 help='For drawing the image with large distance error.')
    parser.add_argument('--batch_size',       type=int,   default=2,      help='Batch size for training.')
    # Checkpoints
    parser.add_argument('--print_freq',       type=int,   default=100,    help='print frequency (default: 200)')
    parser.add_argument('--init_model',       type=str,                   help='The detector model to be initalized.')
    parser.add_argument('--save_path',        type=str,                   help='Folder to save checkpoints and log.')
    # Acceleration
    parser.add_argument('--workers',          type=int,   default=8,      help='number of data loading workers (default: 2)')
    # Random Seed
    parser.add_argument('--rand_seed',        type=int,                   help='manual seed')
    args = parser.parse_args()
    update_config(config, args)
    return args

def main(args):
    logger1, final_output_dir, tb_log_dir = \
        utils.create_logger(config, args.cfg, 'train')
    logger = Logger(args.save_path, "LK train")
    logger1.info(pprint.pformat(args))
    logger1.info(pprint.pformat(config))
    
    cudnn.benchmark = config.CUDNN.BENCHMARK
    cudnn.determinstic = config.CUDNN.DETERMINISTIC
    cudnn.enabled = config.CUDNN.ENABLED
    
     
    # Model Configure Load
    model_config = load_configure(args.model_config, logger)
    dataset_type = get_dataset(config)
    writer_dict = {
        'writer': SummaryWriter(log_dir=tb_log_dir),
        'train_global_steps': 0,
        'valid_global_steps': 0,
    }
    gpus = list(config.GPUS)
    
    lk_config = load_configure(args.lk_config, logger)
    ## Uncomment for loading detector only
    #model = models.get_face_alignment_net(config)
    model = obtain_model(model_config, lk_config, args.num_pts, config)
    logger.log('arguments : {:}'.format(args))
    
    opt_config = load_configure(args.opt_config, logger)
    
    model = torch.nn.DataParallel(model)
    
    criterion = torch.nn.MSELoss(size_average=True).cuda()
    optimizer = utils.get_optimizer(config, model)
    best_nme = 100
    last_epoch = config.TRAIN.BEGIN_EPOCH
    logger.log('criterion : {:}'.format(criterion))
    
    model, criterion = model.cuda(), criterion.cuda()
    
    if isinstance(config.TRAIN.LR_STEP, list):
        scheduler = torch.optim.lr_scheduler.MultiStepLR(
            optimizer, config.TRAIN.LR_STEP,
            config.TRAIN.LR_FACTOR, last_epoch-1
        )
    else:
        scheduler = torch.optim.lr_scheduler.StepLR(
            optimizer, config.TRAIN.LR_STEP,
            config.TRAIN.LR_FACTOR, last_epoch-1
        )
    if config.TRAIN.RESUME: ### Resume works for 1.6.0 torch version for small training
        model_state_file = config.MODEL.PRETRAINED
        if os.path.exists(model_state_file):
            checkpoint = torch.load(model_state_file)
            last_epoch = checkpoint['epoch'] + 1
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            scheduler.load_state_dict(checkpoint['scheduler'])
            logger.log("=> load-ok checkpoint '{:}' (epoch {:}) done" .format(logger.last_info(), checkpoint['epoch']))
    train_data = dataset_type(config, args.heatmap_type, args.data_indicator, is_train=True)
    train_data.load_list(config.DATASET.VIDEO, args.num_pts)
    train_loader = torch.utils.data.DataLoader(
        train_data,
        batch_size=config.TRAIN.BATCH_SIZE_PER_GPU*len(gpus),
        shuffle=config.TRAIN.SHUFFLE,
        num_workers=config.WORKERS,
        pin_memory=config.PIN_MEMORY)
    
    val_loader = torch.utils.data.DataLoader(
        dataset=Face300W_VAL(config,
                             is_train=False),
        batch_size=config.TEST.BATCH_SIZE_PER_GPU*len(gpus),
        shuffle=False,
        num_workers=config.WORKERS,
        pin_memory=config.PIN_MEMORY
    )        
    # Main Training and Evaluation Loop
    start_time = time.time()
    epoch_time = AverageMeter()
    start_epoch = 0
    for epoch in range(last_epoch, config.TRAIN.END_EPOCH):
    
        scheduler.step()
        need_time = convert_secs2time(epoch_time.avg * (opt_config.epochs-epoch), True)
        epoch_str = 'epoch-{:03d}-{:03d}'.format(epoch, opt_config.epochs)
        LRs       = scheduler.get_lr()
        logger.log('\n==>>{:s} [{:s}], [{:s}], LR : [{:.5f} ~ {:.5f}], Config : {:}'.format(time_string(), epoch_str, need_time, min(LRs), max(LRs), opt_config))
    
        # train for one epoch
        train_loss = train(args, train_loader, model, criterion, optimizer, epoch_str, logger, opt_config, lk_config, epoch>=lk_config.start)
        
        # log the results    
        logger.log('==>>{:s} Train [{:}] Average Loss = {:.6f}'.format(time_string(), epoch_str, train_loss))
    
        # evaluate
        nme, predictions = function.validate(config, val_loader, model,
                                             criterion, epoch, writer_dict)
        is_best = nme < best_nme
        best_nme = min(nme, best_nme)
        print("best:", is_best)
        # remember best prec@1 and save checkpoint
        save_path = save_checkpoint({
            'epoch': epoch,
            'args' : deepcopy(args),
            'arch' : model_config.arch,
            'state_dict': model.state_dict(),
            #'detector'  : detector.state_dict(),
            'scheduler' : scheduler.state_dict(),
            'optimizer' : optimizer.state_dict(),
            }, logger.path('model') / '{:}-{:}.pth'.format(model_config.arch, epoch_str), logger)
    
        last_info = save_checkpoint({
            'epoch': epoch,
            'last_checkpoint': save_path,
            }, logger.last_info(), logger)
    
        # measure elapsed time
        epoch_time.update(time.time() - start_time)
        start_time = time.time()

    logger.close()

if __name__ == '__main__':
  args = parse_args()
  main(args)