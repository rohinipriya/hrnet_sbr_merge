# ------------------------------------------------------------------------------
# Copyright (c) Microsoft
# Licensed under the MIT License.
# Created by Tianheng Cheng(tianhengcheng@gmail.com)
# ------------------------------------------------------------------------------

import os
import pprint
import argparse

import torch
import torch.nn as nn
import torch.backends.cudnn as cudnn
from torch.utils.data import DataLoader
from pathlib import Path
import sys
from collections import OrderedDict
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))
sys.path.append("/home/mcw/Rohini/landmarks/training/HRNet-DT-Merge-BK-Matched/lib/")
lib_dir = (Path(__file__).parent / '..' / 'lib').resolve()
if str(lib_dir) not in sys.path: sys.path.insert(0, str(lib_dir))
from lib.config import config, update_config
import torch.onnx
from models.hrnet_onnx import get_face_alignment_net


def parse_args():

    parser = argparse.ArgumentParser(description='Train Face Alignment')

    parser.add_argument('--cfg', help='experiment configuration filename',
                        required=True, type=str)
    parser.add_argument('--model_file', help='model parameters', required=True, type=str)

    args = parser.parse_args()
    update_config(config, args)
    return args


def main():

    args = parse_args()
    
    config.defrost()
    config.MODEL.INIT_WEIGHTS = False
    config.freeze()
    model = get_face_alignment_net(config)

    device = torch.device('cpu')
    print("Model File path: ", args.model_file,)
    # load model
    state_dict = torch.load(args.model_file, map_location=device)
    state_dict = state_dict["state_dict"]
    #print(state_dict.keys())
    new_pretrained_dict = OrderedDict()
    for i, key in enumerate(state_dict):
        new_pretrained_dict[key.replace('module.detector.', '')] = state_dict[key]
    model.load_state_dict(new_pretrained_dict)
    print("Model Loaded!!!!")
    dummy_input = torch.randn(1, 3, 256, 256)    
    torch.onnx.export(model, dummy_input, "./snapshots/CPM-SBR/onnx/hrnet_lk_4g_sbr_peak_e24.onnx")
    print("Saved Onnx Model!")

if __name__ == '__main__':
    main()

