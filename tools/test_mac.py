# ------------------------------------------------------------------------------
# Copyright (c) Microsoft
# Licensed under the MIT License.
# Created by Tianheng Cheng(tianhengcheng@gmail.com)
# ------------------------------------------------------------------------------

import os
import pprint
import argparse

import torch
import torch.nn as nn
import torch.backends.cudnn as cudnn
from torch.utils.data import DataLoader
import sys
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))
import lib.models as models
from lib.config import config, update_config
from lib.utils import utils
from lib.datasets import get_dataset
from lib.core import function
from thop import profile
from thop import clever_format
from ptflops import get_model_complexity_info
from collections import OrderedDict 

def parse_args():

    parser = argparse.ArgumentParser(description='Train Face Alignment')

    parser.add_argument('--cfg', help='experiment configuration filename',
                        required=True, type=str)
    parser.add_argument('--model-file', help='model parameters', required=False, type=str)

    args = parser.parse_args()
    update_config(config, args)
    return args


def main():

    args = parse_args()

    logger, final_output_dir, tb_log_dir = \
        utils.create_logger(config, args.cfg, 'test')

    logger.info(pprint.pformat(args))
    logger.info(pprint.pformat(config))

    cudnn.benchmark = config.CUDNN.BENCHMARK
    cudnn.determinstic = config.CUDNN.DETERMINISTIC
    cudnn.enabled = config.CUDNN.ENABLED

    config.defrost()
    config.MODEL.INIT_WEIGHTS = True
    input_size = config.MODEL.IMAGE_SIZE
    print(input_size)
    config.freeze()
    model = models.get_face_alignment_net(config)


    device = "cpu"
    dummy_input = torch.randn(1, 3, input_size[0],input_size[1]).to(device)

    macs, params = get_model_complexity_info(model, (3, input_size[0], input_size[1]), as_strings=True,
                                           print_per_layer_stat=True, verbose=True)
    print("\n1. MAC using ptflops Library \n")   
    print('{:<30}  {:<8}'.format('Computational complexity: ', macs))
    print('{:<30}  {:<8}'.format('Number of parameters: ', params))

    print("\n\n------------------------------------------------------\n")    
    print("2. MAC using thop Library \n")
    macs, params=profile(model,inputs=(dummy_input,))
    macs, params = clever_format([macs, params], "%.3f")
    print('MAC :',macs,'PARAMS ',params)
    torch.onnx.export(model,dummy_input,"hrnet_dummy_"+macs+"_.onnx") #opset_version=11

if __name__ == '__main__':
    main()
