import cv2
import numpy as np

img = cv2.imread('./pp_input/raw_img_2_1.png')
pts_file = './mask_out/lm_sbr_out_6.txt'
pts = np.loadtxt(pts_file)
#print(pts[0])
for i in range(0,136,2):
    cv2.circle(img, (int(pts[i]), int(pts[i+1])), 1, (0, 0, 255), 1)
cv2.imwrite('hrnet_lk_1.5g_aug_6.png', img)