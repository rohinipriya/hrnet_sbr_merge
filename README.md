# HRNet and Supervision by Registration Merge #

This repository is merge of HRNet and SBR.

### Overview ###
The input is processed in hrnet format with sequence support. The detector output is post processed with SBR's peak finding operation and the obtained landmark points are given to the LK tracker during training.

HRNet: https://github.com/HRNet/HRNet-Facial-Landmark-Detection

SBR: https://github.com/facebookresearch/supervision-by-registration

### Installation ###

pip install requirements.txt

### Training Steps ###
 * 1.5G Model: bash scripts/train_lk_dt_1_5g.sh
 * 4G Model: bash scripts/train_lk_dt.sh

### Testing Steps###
 * 1.5G Model: python tools/test.py --cfg ./experiments/300W/hrnet_w18_small_v1_1_5G.yaml --model-file <checkpoint_file>
 * 4G Model: python tools/test.py --cfg ./experiments/300W/face_alignment_300w_hrnet_w18.yaml --model-file <checkpoint_file>

### ONNX Conversion ###
 * python tools/save_onnx.py --cfg <cfg_path> --model_file <model_path>