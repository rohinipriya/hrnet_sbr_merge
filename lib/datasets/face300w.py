# ------------------------------------------------------------------------------
# Copyright (c) Microsoft
# Licensed under the MIT License.
# Created by Tianheng Cheng(tianhengcheng@gmail.com), Yang Zhao
# ------------------------------------------------------------------------------

import os
from os import path as osp
import random

import torch
import torch.utils.data as data
import pandas as pd
from PIL import Image
import numpy as np

from ..utils.transforms import fliplr_joints, crop, generate_target, transform_pixel
from .parse_utils import parse_video_by_indicator
from .file_utils import load_file_lists
from .point_meta import Point_Meta
from .dataset_utils import pil_loader
import sys
import cv2

class Face300W(data.Dataset):

    def __init__(self, cfg, heatmap_type, data_indicator, is_train=True, transform=None):
        # specify annotation file for dataset
        if is_train:
            self.csv_file = cfg.DATASET.TRAINSET
        else:
            self.csv_file = cfg.DATASET.TESTSET
            

        self.is_train = is_train
        self.transform = transform
        self.data_root = cfg.DATASET.ROOT
        self.input_size = cfg.MODEL.IMAGE_SIZE
        self.output_size = cfg.MODEL.HEATMAP_SIZE
        self.sigma = cfg.MODEL.SIGMA
        self.scale_factor = cfg.DATASET.SCALE_FACTOR
        self.rot_factor = cfg.DATASET.ROT_FACTOR
        self.label_type = cfg.MODEL.TARGET_TYPE
        self.flip = cfg.DATASET.FLIP
        self.video_parser = cfg.DATASET.VIDEO_PARSER
        L, R = parse_video_by_indicator(None, self.video_parser, True)
        self.video_length = L + R + 1
        self.center_idx = L
        self.dataset_name = data_indicator
        self.heatmap_type = heatmap_type
        
        self.NUM_PTS = cfg.MODEL.NUM_JOINTS
        # load annotations
        if self.csv_file:
            self.landmarks_frame = pd.read_csv(self.csv_file)
            
        self.mean = np.array([0.485, 0.456, 0.406], dtype=np.float32)
        self.std = np.array([0.229, 0.224, 0.225], dtype=np.float32)
        self.reset()
        
    def reset(self):
        self.length = 0
        self.datas = []
        self.labels = []
        self.face_sizes = []
        self.boxes = []
        self.scales = []
        self.center_w = []
        self.center_h = []
    
    def __len__(self):
        assert len(self.datas) == self.length, 'The length is not correct : {}'.format(self.length)
        return self.length
    
    def append(self, data, label, box, face_size, scale, c_w, c_h):
        assert osp.isfile(data), 'The image path is not a file : {}'.format(data)
        self.datas.append( data )
        self.labels.append( label )
        self.face_sizes.append( face_size )
        self.boxes.append( box )
        self.scales.append( scale )
        self.center_w.append( c_w )
        self.center_h.append( c_h )
        self.length = self.length + 1
    
    def load_data(self, datas, labels, boxes, face_sizes, scales, center_w, center_h, num_pts):
        print ('[GeneralDataset] load-data {:} datas begin'.format(len(datas)))
        
        for idx, data in enumerate(datas):
            assert isinstance(data, str), 'The type of data is not correct : {}'.format(data)
            assert osp.isfile(datas[idx]), '{} is not a file'.format(datas[idx])
            self.append(datas[idx], labels[idx], boxes[idx], face_sizes[idx], scales[idx], center_w[idx], center_h[idx])
        
        assert len(self.datas) == self.length, 'The length and the data is not right {} vs {}'.format(self.length, len(self.datas))
        assert len(self.labels) == self.length, 'The length and the labels is not right {} vs {}'.format(self.length, len(self.labels))
        assert len(self.face_sizes) == self.length, 'The length and the face_sizes is not right {} vs {}'.format(self.length, len(self.face_sizes))
        print ('Load data done for the general dataset, which has {} images.'.format(self.length))
        
    def load_list(self, file_lists, num_pts):
        datas, labels, boxes, face_sizes = [], [], [], []
        scales, center_w, center_h = [], [], []
        print(file_lists)
        if file_lists != '':
            lists = load_file_lists(file_lists)
            print ('GeneralDataset : load-list : load {:} lines'.format(len(lists)))
            for idx, data in enumerate(lists):
                alls = [x for x in data.split(' ') if x != '']
                
                assert len(alls) == 6 or len(alls) == 7, 'The {:04d}-th line in {:} is wrong : {:}'.format(idx, data)
                datas.append( alls[0] )
                if alls[1] == 'None':
                    labels.append( None )
                else:
                    labels.append( alls[1] )
                box = np.array( [ float(alls[2]), float(alls[3]), float(alls[4]), float(alls[5]) ] )
                boxes.append( box )
                scale = max(box[2] - box[0], box[3] - box[1]) / 200
                scale *= 1.25
                c_w = (box[0] + box[2]) / 2
                c_h = (box[1] + box[3]) / 2
                scales.append(scale)
                center_w.append(c_w)
                center_h.append(c_h)
                if len(alls) == 6:
                    face_sizes.append( None )
                else:
                    face_sizes.append( float(alls[6]) )  
        if self.csv_file != '':
            for idx in range(len(self.landmarks_frame)):
                image_path = os.path.join(self.data_root, self.landmarks_frame.iloc[idx, 0])
                pts = self.landmarks_frame.iloc[idx, 4:].values
                pts = pts.astype('float').reshape(-1, 2)
                datas.append(image_path)
                labels.append(pts)
                face_sizes.append( None )
                boxes.append( None )
                scale = self.landmarks_frame.iloc[idx, 1]
                scale *= 1.25
                center_w.append(self.landmarks_frame.iloc[idx, 2])
                center_h.append(self.landmarks_frame.iloc[idx, 3])
                scales.append(scale)
        self.load_data(datas, labels, boxes, face_sizes, scales, center_w, center_h, num_pts)
        #print(self.boxes)
    
    def __getitem__(self, idx):
        images, is_video_or_not = parse_video_by_indicator(self.datas[idx], self.video_parser, False)
        images = [pil_loader(img) for img in images]
        
        pts = self.labels[idx]
        # obtain the visiable indicator vector
        if pts == 'none': nopoints = True
        else : nopoints = False 
        #nparts = pts.shape[0]
        nparts = self.NUM_PTS
        r = 0
        pp_images = []
        cnt = 0 
        center_list = []
        scale_list = []
        clist = []
        slist = []
        ## Augmentation during training
        r = 0
        random_flip = False
        rand_scale = 1
        if self.is_train:
            rand_scale = random.uniform(1 - self.scale_factor, 1 + self.scale_factor)
            r = random.uniform(-self.rot_factor, self.rot_factor) \
                if random.random() <= 0.6 else 0
            if random.random() <= 0.5 and self.flip:
                random_flip = True
        for img in images:
            #print("img count ", cnt)
            cnt = cnt + 1
            scale = self.scales[idx]
            scale = scale * rand_scale
            center_w = self.center_w[idx]
            center_h = self.center_h[idx]
            center = torch.Tensor([center_w, center_h])
            if self.is_train and random_flip:
                img = np.fliplr(img)
                pts = fliplr_joints(pts, width=img.shape[1], dataset='300W')
                center[0] = img.shape[1] - center[0]
            center_list.append([center_w, center_h])
            scale_list.append(scale)                  
            img = crop(img, center, scale, self.input_size, rot=r)
            pp_images.append(img)

        center_meta = np.array(center_list)
        scale_meta = np.array(scale_list)
        clist.append(torch.Tensor(center_meta))
        slist.append(torch.Tensor(scale_meta))
        target = np.zeros((nparts, self.output_size[0], self.output_size[1]))
        
        if not nopoints:
            tpts = pts.copy()
            for i in range(nparts):
                if tpts[i, 1] > 0:
                    tpts[i, 0:2] = transform_pixel(tpts[i, 0:2]+1, center,
                                           scale, self.output_size, rot=r)
                    target[i] = generate_target(target[i], tpts[i]-1, self.sigma,
                                        label_type=self.label_type)
            visiable = pts[:, 1].astype('bool')

        if nopoints:
            pts = np.zeros((self.NUM_PTS, 2), dtype='int')
            tpts = np.zeros((self.NUM_PTS, 2), dtype='int')
            visiable = (tpts[:, 1] * 0).astype('bool')
        
        mask_heatmap      = np.ones((self.NUM_PTS, 1, 1), dtype='float32')
        mask_heatmap[:self.NUM_PTS, 0, 0] = visiable          
        
        images = []
        count = 0
        for img in pp_images:
            #print(count)
            img = img.astype(np.float32)
            count = count + 1
            #cv2.imwrite("./pp_input/raw_img_" + str(idx) + '_' + str(count) + ".png", img)
            img = (img/255.0 - self.mean) / self.std
            #cv2.imwrite("./pp_input/mstd_img_" + str(idx) + '_' + str(count) + ".png", img)
            img = img.transpose([2, 0, 1])        
            img = torch.Tensor(img)
            images.append(img)
        
        target = torch.Tensor(target)
        mask = torch.from_numpy(mask_heatmap).type(torch.ByteTensor)
        tpts = torch.Tensor(tpts)
        center = torch.Tensor(center)
        torch_index = torch.IntTensor([idx])
        torch_nopoints = torch.ByteTensor( [ nopoints ] )
        video_indicator = torch.ByteTensor( [is_video_or_not] )
        meta = {'index': idx, 'center': torch.stack(clist), 'scale': torch.stack(slist),
                'pts': torch.Tensor(pts), 'tpts': tpts}
        #return img, target, meta
        ### CHECK ORI SIZE VAIRABLE USAGE
        return torch.stack(images), target, mask, meta, tpts, torch_index, torch_nopoints, video_indicator


if __name__ == '__main__':

    pass
    