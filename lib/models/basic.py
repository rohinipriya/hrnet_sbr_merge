# Copyright (c) Facebook, Inc. and its affiliates.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.
#
from .LK import LK
from .hrnet import get_face_alignment_net

def obtain_model(configure, points, config):
  if configure.arch == 'hr_net':
    net = get_face_alignment_net(config)
  else:
    raise TypeError('Unknown type : {:}'.format(configure.arch))
  return net

def obtain_LK(configure, lkconfig, points, config):
  model = obtain_model(configure, points, config)
  lk_model = LK(model, lkconfig, points)
  return lk_model
  #return model